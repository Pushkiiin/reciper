name := "reciper"
version := "0.1"
scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.3.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.0",
  "org.postgresql" % "postgresql" % "42.2.14",

  "com.typesafe.akka" %% "akka-actor" % "2.6.10",
  "com.typesafe.akka" %% "akka-stream" % "2.6.10",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.10",
  "com.typesafe.akka" %% "akka-http" % "10.2.1",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.2.1",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.2.1",

  "org.scalaj" %% "scalaj-http" % "2.4.2",

  "org.scalatest" %% "scalatest" % "3.2.0" % Test

)
package dishcrud

case class Dish(name: String, ingredients: String, recipe: String)

package dishcrud

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Route
import spray.json._
import spray.json.RootJsonFormat
import spray.json.DefaultJsonProtocol._

import scala.util.{Failure, Success}


object Router {
  implicit val clientJsonFormat: RootJsonFormat[Dish] = jsonFormat3(Dish)

  private val getDishes = (get & path("dishes")) {
    onComplete(Service.getAll) {
      case Success(value) => complete(value.toJson)
      case Failure(_) => complete(HttpResponse(StatusCodes.InternalServerError, entity = "Something went wrong"))
    }
  }
  private val postDish = (post & path("dishes")){
    entity(as[Dish]) { dish =>
      onComplete(Service.add(dish)) {
        case Success(_) => complete(HttpResponse(StatusCodes.OK, entity = "Dish added to db"))
        case Failure(_) => complete(HttpResponse(StatusCodes.InternalServerError, entity = "Something went wrong"))
      }
    }
  }
  private val getByName = (get & path("findByName")) {
    parameter("name") { name =>
      onComplete(Service.getByName(name)) {
        case Success(dish) => complete(dish.toJson)
        case Failure(_) => complete(HttpResponse(StatusCodes.InternalServerError, entity = "Something went wrong"))
      }
    }
  }
  private val deleteByName = (delete & path("deleteByName")){
    parameter("name") { name =>
      onComplete(Service.deleteByName(name)) {
        case Success(_) => complete(HttpResponse(StatusCodes.OK, entity = "Dish removed from db"))
        case Failure(_) => complete(HttpResponse(StatusCodes.InternalServerError, entity = "Something went wrong"))
      }
    }
  }
  private val getByIngredients = (get & path("getByIngredients")) {
    parameter("ingrs") { ingrs =>
      onComplete(Service.getByIngredients(ingrs.split(" |,").filter(!_.isEmpty))) {
        case Success(value) => complete(value.toJson)
        case Failure(_) => complete(HttpResponse(StatusCodes.InternalServerError, entity = "Something went wrong"))
      }
    }
  }

  val routes: Route = getDishes ~ postDish ~ getByName ~ deleteByName ~ getByIngredients

}

package dishcrud

import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

object Service {
  def getAll: Future[Seq[Dish]] = DataAccess.getAll
  def getByName(name: String): Future[Seq[Dish]] = DataAccess.getByName(name)
  def add(dish: Dish): Future[Int] = DataAccess.add(dish)
  def deleteByName(name: String): Future[Int] = DataAccess.deleteByName(name)

  def getByIngredients(ingrs: Seq[String]): Future[Seq[Dish]] = for {
    allDishes <- DataAccess.getAll
  } yield allDishes.filter(dish => ingrs.forall(i => dish.ingredients contains i))


}

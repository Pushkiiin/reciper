package dishcrud

import akka.actor.ActorSystem
import akka.http.scaladsl.Http

import scala.concurrent.ExecutionContext


object Main {
  def main(args: Array[String]): Unit = {
    implicit val actorSystem: ActorSystem = ActorSystem()
    implicit val ec: ExecutionContext = actorSystem.dispatcher

    Http().newServerAt("localhost", 8080).bind(Router.routes)
    println("Server online at http://localhost:8080/")
  }
}




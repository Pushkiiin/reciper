package dishcrud

import slick.dbio.Effect
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape

import scala.concurrent.Future


object DataAccess {

  class DishesTable(tag: Tag) extends Table[Dish](tag, "dishes") {
    def name: Rep[String] = column("name")
    def ingredients: Rep[String] = column("ingredients")
    def recipe: Rep[String] = column("recipe")
    override def * : ProvenShape[Dish] = (name, ingredients, recipe).mapTo[Dish]
  }

  object DishesQueryRepo {
    val AllDishes = TableQuery[DishesTable]
    def getAll: DBIOAction[Seq[Dish], NoStream, Effect.Read] = AllDishes.result
    def getByName(name: String): DBIOAction[Seq[Dish], NoStream, Effect.Read] = AllDishes.filter(_.name === name).result
    def add(dish: Dish): DBIOAction[Int, NoStream, Effect.Write] = AllDishes += dish
    def deleteByName(name: String): DBIOAction[Int, NoStream, Effect.Write] = AllDishes.filter(_.name === name).delete
  }


  val db = Database.forConfig("cookbookdb")
  def getAll: Future[Seq[Dish]] = db.run(DishesQueryRepo.getAll)
  def getByName(name: String): Future[Seq[Dish]] = db.run(DishesQueryRepo.getByName(name))
  def add(dish: Dish): Future[Int] = db.run(DishesQueryRepo.add(dish))
  def deleteByName(name: String): Future[Int] = db.run(DishesQueryRepo.deleteByName(name))
}

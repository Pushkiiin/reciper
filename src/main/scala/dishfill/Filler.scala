package dishfill

import akka.actor.ActorSystem
import dishcrud.{Dish, Service}
import scalaj.http.{Http, HttpResponse}

import scala.concurrent.ExecutionContext

object Filler {
  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = actorSystem.dispatcher

  def fill(amount: Int): Unit = {
    for (i <- 1 to amount) {
      val response: HttpResponse[String] =
        Http("https://api.edamam.com/search?q=pepper&app_id=8eaaf856&app_key=e306560b5111917c12830c900ee1256a&from=" + i + "&to=" + (i + 1)).asString
      val data = response.body

      val nameParsed = JsonParser.parseName(data).get
      val ingrsParsed = JsonParser.parseIngrs(data).get
      val recipeParsed = JsonParser.parseRecipe(data).get

      Service.add(Dish(nameParsed, ingrsParsed, recipeParsed))
    }
  }

}

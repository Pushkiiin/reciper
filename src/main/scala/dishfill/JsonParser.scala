package dishfill

object JsonParser {
  def parseName(toParse: String): Option[String] = {
    val nameRegex = java.util.regex.Pattern.compile("^.*?(\"label\":\\\".*?\\\").*")
    val nameMatcher = nameRegex.matcher(toParse)
    if (nameMatcher.find) {
      val hit = nameMatcher.group(1)
      val nameParsed = hit.substring(9, hit.length - 1)
      Option(nameParsed)
    } else {
      Option.empty
    }
  }

  def parseIngrs(toParse: String): Option[String] = {
    val ingrsRegex = java.util.regex.Pattern.compile(".*(\"ingredientLines\":\\[.*?\\]).*")
    val ingrMatcher = ingrsRegex.matcher(toParse)
    if (ingrMatcher.find) {
      val hit = ingrMatcher.group(1)
      val ingrParsed = hit.substring(19, hit.length - 1)
      Option(ingrParsed)
    } else {
      Option.empty
    }
  }

  def parseRecipe(toParse: String): Option[String] = {
    val recipeRegex = java.util.regex.Pattern.compile("^.*?(\"url\":\\\".*?\\\").*")
    val recipeMatcher = recipeRegex.matcher(toParse)
    if (recipeMatcher.find) {
      val hit = recipeMatcher.group(1)
      val recipeParsed = hit.substring(7, hit.length - 1)
      Option(recipeParsed)
    } else {
      Option.empty
    }
  }
}

import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import dishcrud.{Dish, Service}

class ServiceDbIntegrationTest extends AsyncFlatSpec with Matchers {
  val testDish = Dish("testName", "testIngrs", "testRecipe")
  val testDish2 = Dish("testName2", "testIngrs", "testRecipe2")
  val testDish3 = Dish("testName3", "wrongIngrs", "testRecipe3")

  it should "succesfully add dish to database" in {
    Service.add(testDish).map {
      _ shouldBe 1
    }
  }

  it should "succesfully get dish from database" in {
    Service.getByName("testName").map(_.distinct shouldBe Seq(testDish))
  }

  it should "succesfully delete dish from database" in {
    Service.deleteByName("testName").map(_ shouldBe 1)
  }

  it should "succesfully get dishes by ingredients" in {
    Service.getByIngredients(Seq("cheese", "bread")).map(_.map(_.name) shouldBe Seq("burger", "sandwitch"))
  }
}